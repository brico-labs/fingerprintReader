#include <Adafruit_Fingerprint.h>
#include <SoftwareSerial.h>

class PacketBatch {
public:
  Adafruit_Fingerprint_Packet* packets;
  PacketBatch(uint8_t size){
    packets = new Adafruit_Fingerprint_Packet[12];
  }

  ~PacketBatch(){
    delete [] packets;
  }
};

class FingerprintSensor {
public:
  FingerprintSensor();
  void begin();
  int8_t getFingerprintID();
  int8_t fingerprintEnroll();
  int8_t fingerprintEnroll(uint16_t id);
  int8_t deleteFinger(uint16_t id);
  int16_t getTemplateCount();
  uint8_t downloadFingerprintTemplate(uint16_t id, PacketBatch* batch);
  uint8_t uploadFingerprintTemplate(uint16_t id, const PacketBatch& batch);
  void populateDataPacket(Adafruit_Fingerprint_Packet * p, uint16_t length, uint8_t * data, bool last = false);


private:
  const uint8_t RX = D3;
  const uint8_t TX = D4;
  SoftwareSerial serial;
  Adafruit_Fingerprint finger;
  int8_t wait4Finger();
  int8_t trainModel(uint8_t n);
  uint8_t deleteFingerprint(uint8_t id);
  
};