#include "FingerprintSensor/FingerprintSensor.h"
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include "config/config.h"
#include "StatusLeds/StatusLeds.h"
#include <ESP8266httpUpdate.h>
#include <ArduinoJson.h>
#include "FS.h"
#include "Clock/Clock.h"

const short BUTTON = A0;
const short DOOR = D8;
const short CLOSE_SENSOR = D1;
const short PIR_SENSOR = D2;
const char* QUEUE_FILENAME = "msg_queue.json";
unsigned long lastTimeUpdate = 0;
unsigned long lastUpdateTime = 0;
unsigned long lastCheckQueue = 0;
unsigned long lastOpenTeleTime = 0;
unsigned long lastMovementTeleTime = 0;
unsigned long lastOpenTime = 0;
unsigned long lastPingTime = 0;
bool pingLedStatus = LOW;
bool doorOpen = false;
bool movementDetected = false;
uint8_t mqttFailed = 0;

FingerprintSensor fingerSensor;
WiFiClient wifiClient;
PubSubClient client(wifiClient);

String topicCmndOpen = String("/cmnd/lock/") + String(ESP.getChipId()) + "/open";
String topicCmndAdd = String("/cmnd/lock/") + String(ESP.getChipId()) + "/add";
String topicCmndRemove = String("/cmnd/lock/") + String(ESP.getChipId()) + "/remove";
String topicCmndStatus = String("/cmnd/lock/") + String(ESP.getChipId()) + "/status";
String topicCmndDownload = String("/cmnd/lock/") + String(ESP.getChipId()) + "/download";
String topicCmndUpload = String("/cmnd/lock/") + String(ESP.getChipId()) + "/upload/#";
String topicStatEnrolling = String("/stat/lock/") + String(ESP.getChipId()) + "/enrollingId";
String topicStatOpen = String("/stat/lock/") + String(ESP.getChipId()) + "/open";
String topicStatDenied = String("/stat/lock/") + String(ESP.getChipId()) + "/denied";
String topicStatStatus5 = String("/stat/lock/") + String(ESP.getChipId()) + "/STATUS5";
String topicStatStatus = String("/stat/lock/") + String(ESP.getChipId()) + "/status";
String topicTeleOld = String("/tele/lock/") + String(ESP.getChipId()) + "/old";
String topicTeleStatus = String("/tele/lock/") + String(ESP.getChipId()) + "/status";
String topicStatEnrrollingHash = String("/stat/lock/") + String(ESP.getChipId()) + "/enrollingHash";
String topicStatUpload = String("/stat/lock/") + String(ESP.getChipId()) + "/upload";
String topicStatDownload = String("/stat/lock/") + String(ESP.getChipId()) + "/download";

void open();
void reconnect();
void callback(char* topic, byte* payload, unsigned int length);

void setup()
{
  SPIFFS.begin();
  Serial.begin(115200);
  fingerSensor.begin();
  StatusLeds::begin();
  pinMode(DOOR, OUTPUT);
  pinMode(BUZZER, OUTPUT);
  pinMode(CLOSE_SENSOR, INPUT_PULLUP);
  pinMode(PIR_SENSOR, INPUT);

  // setup wifi connection
  WiFi.mode(WIFI_STA);
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
  if (!WiFi.getAutoConnect()) { WiFi.setAutoConnect(true); } 

  while (WiFi.status() != WL_CONNECTED)
  {
    static int i = 0;
    i++;
    if (i>120) // 1 min
      ESP.restart();
    Serial.print(".");
    delay(500);
  }

  Serial.println("Connected to network!");

  int i = 0;
  while (!Clock::updateTime() && i++ < 3)
  {
    Serial.println("Unable to get time. ");
    delay(500);
  }
  lastTimeUpdate = millis();

  client.setServer(MQTT_ADDR, 1883);
  client.setCallback(callback);
  delay(100);
  reconnect();
}

void loop()
{
  int code = fingerSensor.getFingerprintID();

  if (code >= 0) { // Fingerprint found
    char payload[3] = "";
    itoa(code,payload, 10);

    if (client.connected()) {
      client.publish(topicStatOpen.c_str(), payload);
    }
    else {
      // TODO: This needs to be debuged and refactored
      File file = SPIFFS.open(QUEUE_FILENAME, "a");
      if (!file)
      {
        Serial.println("Error while reading config file.");
      } else {
        StaticJsonDocument<200> root;
        root["time"] = Clock::getUnixTime();
        root["command"] = "open";
        root["id"] = payload;
        serializeJson(root, Serial);
        serializeJson(root, file);
        file.println();
        file.close();
        Serial.println("Storing message in Queue");
      }
    }

    open();
  }
  else if (code == -2) { // Fingeprint not found
    if (client.connected()) {
      char payload[1] = "";
      client.publish(topicStatDenied.c_str(), payload);
    }
    else {
      // TODO: we have to persist this and send log when the connection is available
    }

    StatusLeds::on(RED_LED);
    StatusLeds::beep(4, 100);
    delay(200);
    StatusLeds::off(RED_LED);
  }

  if (!client.connected()) { // Reconnect to MQTT if necesary
    reconnect();
  } else {
    if (millis() - lastPingTime > 5000) {
      pingLedStatus = !pingLedStatus;
      if (pingLedStatus){
        StatusLeds::on(GREEN_LED);
        lastPingTime = (millis() - 5000) + 300;
      } else {
        StatusLeds::off(GREEN_LED);
        lastPingTime = millis();
      }
    }
  }

  client.loop(); // MQTT update

  if (millis() - lastUpdateTime > 60 * 60 * 1000) {
    lastUpdateTime = millis();
    String url = "http://radon.4m1g0.com/update/";
    url += String(ESP.getChipId()) + String(".bin");
    WiFiClient client;
    t_httpUpdate_return ret = ESPhttpUpdate.update(client, url);

    switch (ret) {
      case HTTP_UPDATE_FAILED:
        Serial.printf("HTTP_UPDATE_FAILD Error (%d): %s\n", ESPhttpUpdate.getLastError(), ESPhttpUpdate.getLastErrorString().c_str());
        break;

      case HTTP_UPDATE_NO_UPDATES:
        Serial.println("HTTP_UPDATE_NO_UPDATES");
        break;

      case HTTP_UPDATE_OK:
        Serial.println("HTTP_UPDATE_OK");
        break;
    }
  }

  short isOpen = digitalRead(CLOSE_SENSOR);
  // send status when it changes or each minute or at startup
  if (isOpen != doorOpen || millis() - lastOpenTeleTime > 60000 || lastOpenTeleTime == 0) {
    Serial.println("Send Tele");
    lastOpenTeleTime = millis();
    char* topic;
    if (isOpen != doorOpen)
      topic = (char *) topicStatStatus.c_str();
    else 
      topic = (char *) topicTeleStatus.c_str();

    doorOpen = isOpen;

    if (client.connected()) {
      if (doorOpen) 
        client.publish(topic, "1");
      else
        client.publish(topic, "0");
    }
    else {
      // TODO: we have to persist this and send log when the connection is available
    }
  }

  short movement = digitalRead(PIR_SENSOR);
  // send movement sensor status when it changes or each minute or at startup
  if (movement != movementDetected || millis() - lastMovementTeleTime > 300000 || lastMovementTeleTime == 0) {
    Serial.println("Send Tele");
    lastMovementTeleTime = millis();
    String topic;
    if (movement != movementDetected)
      topic = String("/stat/lock/") + String(ESP.getChipId()) + "/movement";
    else 
      topic = String("/tele/lock/") + String(ESP.getChipId()) + "/movement";
    movementDetected = movement;

    if (client.connected()) {
      if (movementDetected) 
        client.publish(topic.c_str(), "1");
      else
        client.publish(topic.c_str(), "0");
    }
    else {
      // TODO: we have to persist this and send log when the connection is available
    }
  }

  // update time via NTP
  if ((unsigned long)(millis() - lastTimeUpdate) > 1000*60*60*5)
  {
    Serial.println("Clock update...");
    Clock::updateTime();
    lastTimeUpdate = millis();
  }

  // check queue to free it
  if ((unsigned long)(millis() - lastCheckQueue) > 1000*60) {
    lastCheckQueue = millis();
    File file = SPIFFS.open(QUEUE_FILENAME, "r");

    if (file && client.connected())
    {
      Serial.print("Sending queued information.");
      while (file.available()) {
        String message = file.readStringUntil('\n');
        client.publish(topicTeleOld.c_str(), message.c_str());
      }
    }

    file.close();
    SPIFFS.remove(QUEUE_FILENAME);
  }

  if (analogRead(BUTTON) < 90){
    Serial.println("Button pressed!");
    open();
  }
}

void open() {
  if (millis() - lastOpenTime < 3000) {
    return;
  }
  lastOpenTime = millis();
  digitalWrite(DOOR, HIGH);
  digitalWrite(BUZZER, HIGH);
  digitalWrite(GREEN_LED, HIGH);
  delay(1000);
  digitalWrite(GREEN_LED, LOW);
  digitalWrite(BUZZER, LOW);
  digitalWrite(DOOR, LOW);
}

void reconnect() {
  static unsigned long lastConnection = 0;

  if (client.connected() || millis() - lastConnection < 3000)
    return;
  
  lastConnection = millis();

  wifiClient = WiFiClient();               // Wifi Client reconnect issue 4497 (https://github.com/esp8266/Arduino/issues/4497) (From Tasmota)
  client.setClient(wifiClient);

  if (client.connect(String(ESP.getChipId()).c_str(), MQTT_USER, MQTT_PASSWORD)) {
    Serial.println("MQTT connected");
    client.subscribe(topicCmndOpen.c_str());
    client.subscribe(topicCmndAdd.c_str());
    client.subscribe(topicCmndRemove.c_str());
    client.subscribe(topicCmndStatus.c_str());
    client.subscribe(topicCmndDownload.c_str());
    client.subscribe(topicCmndUpload.c_str());
    mqttFailed = 0;
  } 
  else {  
    StatusLeds::on(RED_LED);
    delay(100);
    StatusLeds::off(RED_LED);
    Serial.println("MQTT Connection failed");
    if (mqttFailed > 20) { // if failed for 1 minute, reset
      StatusLeds::beep(10, 80);
      ESP.restart();
    }
    mqttFailed++;
  }
}

bool compareTopics(char* topica, char* topicb){
  Serial.println("Compare: ");
  Serial.println(topica);
  Serial.println(topicb);
  for (int i = 0; ; i++){
    if (topica[i] == '\0' || topicb[i] == '\0')
      return true;
    
    if (topica[i] != topicb[i])
      return false;
  }

  return true; // should never get here
}


void callback(char* topic, byte* payload, unsigned int length) {
  if (!strcmp(topic, topicCmndOpen.c_str())) {
    Serial.println("Opening from MQTT Command");
    open();
  } 
  else if (!strcmp(topic, topicCmndAdd.c_str())) {
    Serial.println("Adding new fingerprint to the system");

    if (length > 2) {
      Serial.println("Mensaje mqtt incorrecto");
      return;
    }

    int8_t id = 0;
    if (length == 0) { // enroll finger in the next free position
      id = fingerSensor.fingerprintEnroll();
    } 
    else { // enroll finger in a specific id position (override)
      char payloadText[3];
      memcpy(payloadText, payload, length);
      payloadText[length] = '\0';
      id = atoi(payloadText);
      if (id < 1 || id > 99)
        return;
      
      id = fingerSensor.fingerprintEnroll(id);
    }
    
    Serial.print("ID: "); Serial.println(id);
    char payload[4] = "";
    itoa(id,payload, 10);
    long init_time = millis();
    while (!client.connected()) { // if disconnected from server try to reconnect by all means
      reconnect();
      delay(200);
      if (millis() - init_time > 3000) {
        break;
      }
    }
    if (client.connected()) {
      client.publish(topicStatEnrolling.c_str(), payload);
    } 
    else {
      // If it is not possible to notify the server, the finger is deleted so that the data
      // and ids are consistent thoughout the system
      fingerSensor.deleteFinger(id);
    }
  }
  else if (!strcmp(topic, topicCmndRemove.c_str())) {
    if (length > 2) {
      Serial.println("Mensaje mqtt incorrecto");
      return;
    }

    char payloadText[3];
    memcpy(payloadText, payload, length);
    payloadText[length] = '\0';
    if (!fingerSensor.deleteFinger(atoi(payloadText))) {
      Serial.print("Huella eliminada: "); Serial.println(payloadText);
    } 
    else {
      Serial.print("Hubo un error al eliminar la huella: ");Serial.println(payloadText);
    }
  } 
  else if (!strcmp(topic, topicCmndStatus.c_str())) {
    String topic = String("/stat/lock/") + String(ESP.getChipId()) + "/STATUS";

    String reply = "{\"StatusNET\":{\"Hostname\":\"";
    reply += WiFi.hostname();
    reply += "\", \"IPAddress\":\"";
    reply += WiFi.localIP().toString();
    reply += "\", \"Gateway\":\"";
    reply += WiFi.gatewayIP().toString();
    reply += "\", \"Subnetmask\":\"";
    reply += WiFi.subnetMask().toString();
    reply += "\", \"DNSServer\":\"";
    reply += WiFi.dnsIP().toString();
    reply += "\", \"Mac\":\"";
    reply += WiFi.macAddress();
    reply += "\"}}";

    if (client.connected()) {
      client.publish(topicStatStatus5.c_str(), reply.c_str());
    } 
  }
  else if (!strcmp(topic, topicCmndDownload.c_str())) {
    if (length > 2) {
      Serial.println("Mensaje mqtt incorrecto");
      return;
    }

    char payloadText[3];
    memcpy(payloadText, payload, length);
    payloadText[length] = '\0';
    PacketBatch batch(12);
    int stat = -1;
    int i = 0;
    while (stat && i++ < 15) {
      stat = fingerSensor.downloadFingerprintTemplate(atoi(payloadText), &batch);
      delay(5);
    }
    
    if (i == 15){
      Serial.println("Unable to get fingerprint template. 15 atemps failed.");
    }
    else if (!stat) {
      if (client.connected()) {
        client.publish(topicStatDownload.c_str(), "1");
        for (int i = 0; i < 12; i++) {
          client.publish((topicStatEnrrollingHash + String(i)).c_str(), batch.packets[i].data, batch.packets[i].length);
        }
      }
    } else {
      client.publish(topicStatDownload.c_str(), "0");
    }
  }
  else { // upload fingerprint
    if (compareTopics(topic, (char *)topicCmndUpload.substring(0, topicCmndUpload.length()-1).c_str())) {
      uint8_t idSize = strlen(topic) - topicCmndUpload.length() + 1;
      char charId[idSize+1];
      strncpy(charId, topic+(strlen(topic)-idSize), idSize+1);
      uint8_t id = atoi(charId);
      itoa(id, charId, 10);

      static PacketBatch batch(12);
      if (id == 12) {
        char payloadText[3];
        memcpy(payloadText, payload, length);
        payloadText[length] = '\0';
        if (fingerSensor.uploadFingerprintTemplate(atoi(payloadText), batch) == FINGERPRINT_OK) {
          client.publish(topicStatUpload.c_str(), "1");
        }
        else {
          client.publish(topicStatUpload.c_str(), "0");
        }
      }
      else {
        if (id > 12) {
          Serial.println("Error received more than 12 packets");
          return;
        }
        fingerSensor.populateDataPacket(&batch.packets[id], length, payload, id==11);
      }
    }
    
  }

  Serial.print("Message [");
  Serial.print(topic);
  Serial.print("] ");
  for (unsigned int i=0;i<length;i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();
}
