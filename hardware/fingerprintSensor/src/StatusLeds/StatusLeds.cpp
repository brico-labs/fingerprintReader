#include "StatusLeds.h"

void StatusLeds::begin(){
  pinMode(GREEN_LED, OUTPUT);
  pinMode(RED_LED, OUTPUT);
}

void StatusLeds::off(Leds led){
  digitalWrite(led, LOW);
}

void StatusLeds::on(Leds led){
  digitalWrite(led, HIGH);
}

void StatusLeds::blinkBoth(){
  for (int i = 0; i < 3; i++) {
    digitalWrite(GREEN_LED, LOW);
    digitalWrite(RED_LED, LOW);
    delay(200);
    digitalWrite(GREEN_LED, HIGH);
    digitalWrite(RED_LED, HIGH);
    delay(200);
  }
  
  digitalWrite(GREEN_LED, LOW);
  digitalWrite(RED_LED, LOW);
}

void StatusLeds::beep(uint8_t times, uint16_t interval){
  for (int i = 0; i < times; i++) {
      digitalWrite(BUZZER, HIGH);
      delay(interval);
      digitalWrite(BUZZER, LOW);
      delay(interval);
    }
}